﻿/*/////////////////////////////////////////////////////////////////////
	Version 1.00 
	Application for sorting images flagged as "chips". 
*//////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace sortChips
{
    public partial class Form1 : Form
    {
        private string outputDir;
        private string dirChoice;
        private string[] fileLocations;
        private int progress; // index in fileLocations inventory/ index of image being interacted with/ number of previously viewed images
        private int size; // number of images in chosen directory
        private Bitmap display;
        private string[] outputSites;
        private string bigChips;
        private string smallChips;
        private string coastChips;
        private string landChips;
        private string cloudChips;
        private string wakeChips;
        private string waterChips;
        private string potentialChips;
        
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            info.Text = "Choose a directory to begin.";
        }

		// shows a dialog to choose a directory to view images from
        private void ChooseDir_Click(object sender, EventArgs e)
        {
            if (Browser.ShowDialog() == DialogResult.OK)
            {
                dirChoice = Browser.SelectedPath;
            }
            if (dirChoice != null)
            {
                StartProcess();
            }
        }

        private void StartProcess()
        {
            //load input folder
            outputDir = AppDomain.CurrentDomain.BaseDirectory; 
            fileLocations = Directory.GetFiles(dirChoice,"*.jpg");
            size = fileLocations.Length;
            progress = 0; // and will be incremented when an image is saved or skipped
            DisplayText();
            NewImage();

            // create output folders
            bigChips = outputDir + "\\big";
            Directory.CreateDirectory(bigChips);
            smallChips = outputDir + "\\small";
            Directory.CreateDirectory(smallChips);
            coastChips = outputDir + "\\coast";
            Directory.CreateDirectory(coastChips);
            landChips = outputDir + "\\land";
            Directory.CreateDirectory(landChips);
            cloudChips = outputDir + "\\cloud";
            Directory.CreateDirectory(cloudChips);
            wakeChips = outputDir + "\\wake";
            Directory.CreateDirectory(wakeChips);
            waterChips = outputDir + "\\water";
            Directory.CreateDirectory(waterChips);
            potentialChips = outputDir + "\\potential";
            Directory.CreateDirectory(potentialChips);
            outputSites = new string[8] { bigChips, smallChips, coastChips, landChips, cloudChips, wakeChips, waterChips, potentialChips};
        }

		// all 8 button methods copy the current chip to the selected output folder
        private void Button1_Click(object sender, EventArgs e)
        {
            CopyImage(bigChips);
        }
   
        private void Button2_Click(object sender, EventArgs e)
        {
            CopyImage(coastChips);
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            CopyImage(landChips);
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            CopyImage(waterChips);
        }

		private void Button5_Click(object sender, EventArgs e)
        {
            CopyImage(smallChips);
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            CopyImage(cloudChips);
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            CopyImage(wakeChips);
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            CopyImage(potentialChips);
        }

        private void Skip_Click(object sender, EventArgs e)
        {
            if (dirChoice != null)
            {
                progress++;
                NewImage();
            }
        }

		// undoes the previous choice - removes image from directory where it was placed and 
		//displays it again
        private void Back_Click(object sender, EventArgs e)
        {
            if (progress > 0)
            {
                // backtrack progress
                progress -= 1;
                // show previous image and text
                NewImage();
                // remove this image from all directories
                for (int i = 0; i < outputSites.Length; i++)
                {
                    if (File.Exists(Path.Combine(outputSites[i], Path.GetFileName(fileLocations[progress]))))
                    {
                        File.Delete(Path.Combine(outputSites[i], Path.GetFileName(fileLocations[progress])));
                    }
                }
            }
        }

		// copies the chip currently displayed to the directory passed as an argument,
		// then shows the next chip
        private void CopyImage(string dest)
        {
            if (dirChoice != null)
            { 
                System.IO.File.Copy(fileLocations[progress], dest + "\\" + Path.GetFileName(fileLocations[progress]), true);
                progress++;
                NewImage();
            }
        }
		
		// removes chip currently displayed, displayes next chip, and updates progress text
        private void NewImage()
        {
            DisplayText();
            if (display != null)
            {
                display.Dispose();
            }
            display = new Bitmap(fileLocations[progress]);
            imageBox.Image = (Image)display;
        }

		// displays the number of chips already sorted with the total number to sort, the name of
		// the current chip, and the current directory
        private void DisplayText()
        {
            info.Text = "Sorted: " + progress + " out of " + size + ".\n";
            info.Text += Path.GetFileName(fileLocations[progress]);
            dir.Text = "Directory: " + dirChoice + "\n";
        }

        // unused methods
        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }
        private void PictureBox2_Click(object sender, EventArgs e)
        {
            
        }

        private void outputFolderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {
            
        }

        private void Info_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
