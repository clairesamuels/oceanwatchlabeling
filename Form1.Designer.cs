﻿namespace sortChips
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.Button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.imageBox = new System.Windows.Forms.PictureBox();
			this.Skip = new System.Windows.Forms.Button();
			this.info = new System.Windows.Forms.RichTextBox();
			this.chooseDir = new System.Windows.Forms.Button();
			this.dir = new System.Windows.Forms.TextBox();
			this.Browser = new System.Windows.Forms.FolderBrowserDialog();
			this.back = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.imageBox)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Button1
			// 
			this.Button1.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.Button1.Location = new System.Drawing.Point(368, 133);
			this.Button1.Name = "Button1";
			this.Button1.Size = new System.Drawing.Size(206, 53);
			this.Button1.TabIndex = 0;
			this.Button1.Text = "Big Target";
			this.Button1.UseVisualStyleBackColor = false;
			this.Button1.Click += new System.EventHandler(this.Button1_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.button2.Location = new System.Drawing.Point(368, 251);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(206, 53);
			this.button2.TabIndex = 1;
			this.button2.Text = "Coast";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.Button2_Click);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.button3.Location = new System.Drawing.Point(368, 192);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(206, 53);
			this.button3.TabIndex = 2;
			this.button3.Text = "Land";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.Button3_Click);
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.button4.Location = new System.Drawing.Point(368, 310);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(206, 53);
			this.button4.TabIndex = 3;
			this.button4.Text = "Water";
			this.button4.UseVisualStyleBackColor = false;
			this.button4.Click += new System.EventHandler(this.Button4_Click);
			// 
			// imageBox
			// 
			this.imageBox.Location = new System.Drawing.Point(12, 133);
			this.imageBox.Name = "imageBox";
			this.imageBox.Size = new System.Drawing.Size(343, 305);
			this.imageBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.imageBox.TabIndex = 4;
			this.imageBox.TabStop = false;
			this.imageBox.Click += new System.EventHandler(this.PictureBox1_Click);
			// 
			// Skip
			// 
			this.Skip.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.Skip.Location = new System.Drawing.Point(672, 385);
			this.Skip.Name = "Skip";
			this.Skip.Size = new System.Drawing.Size(116, 53);
			this.Skip.TabIndex = 5;
			this.Skip.Text = "Skip\r\n";
			this.Skip.UseVisualStyleBackColor = false;
			this.Skip.Click += new System.EventHandler(this.Skip_Click);
			// 
			// info
			// 
			this.info.BackColor = System.Drawing.SystemColors.Control;
			this.info.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.info.CausesValidation = false;
			this.info.DetectUrls = false;
			this.info.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.info.Location = new System.Drawing.Point(12, 15);
			this.info.Name = "info";
			this.info.ReadOnly = true;
			this.info.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
			this.info.Size = new System.Drawing.Size(343, 112);
			this.info.TabIndex = 11;
			this.info.Text = "";
			this.info.TextChanged += new System.EventHandler(this.Info_TextChanged);
			// 
			// chooseDir
			// 
			this.chooseDir.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.chooseDir.Location = new System.Drawing.Point(432, 15);
			this.chooseDir.Name = "chooseDir";
			this.chooseDir.Size = new System.Drawing.Size(356, 49);
			this.chooseDir.TabIndex = 13;
			this.chooseDir.Text = "Choose New Directory\r\n";
			this.chooseDir.UseVisualStyleBackColor = false;
			this.chooseDir.Click += new System.EventHandler(this.ChooseDir_Click);
			// 
			// dir
			// 
			this.dir.BackColor = System.Drawing.SystemColors.Control;
			this.dir.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.dir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.dir.Location = new System.Drawing.Point(12, 79);
			this.dir.Multiline = true;
			this.dir.Name = "dir";
			this.dir.ReadOnly = true;
			this.dir.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.dir.Size = new System.Drawing.Size(776, 26);
			this.dir.TabIndex = 14;
			// 
			// Browser
			// 
			this.Browser.RootFolder = System.Environment.SpecialFolder.UserProfile;
			this.Browser.SelectedPath = "C:\\Users\\ClaireSamuels\\source\\repos\\OceanWatchLabeling\\boatcnn\\labeled\\all";
			// 
			// back
			// 
			this.back.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.back.Location = new System.Drawing.Point(536, 385);
			this.back.MaximumSize = new System.Drawing.Size(116, 53);
			this.back.MinimumSize = new System.Drawing.Size(116, 53);
			this.back.Name = "back";
			this.back.Size = new System.Drawing.Size(116, 53);
			this.back.TabIndex = 15;
			this.back.Text = "Back\r\n";
			this.back.UseVisualStyleBackColor = false;
			this.back.Click += new System.EventHandler(this.Back_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button8);
			this.panel1.Controls.Add(this.button7);
			this.panel1.Controls.Add(this.button6);
			this.panel1.Controls.Add(this.button5);
			this.panel1.Location = new System.Drawing.Point(368, 133);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(420, 230);
			this.panel1.TabIndex = 16;
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.button8.Location = new System.Drawing.Point(214, 177);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(206, 53);
			this.button8.TabIndex = 4;
			this.button8.Text = "Potential";
			this.button8.UseVisualStyleBackColor = false;
			this.button8.Click += new System.EventHandler(this.Button8_Click);
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.button7.Location = new System.Drawing.Point(214, 118);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(206, 53);
			this.button7.TabIndex = 3;
			this.button7.Text = "Wake";
			this.button7.UseVisualStyleBackColor = false;
			this.button7.Click += new System.EventHandler(this.Button7_Click);
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.button6.Location = new System.Drawing.Point(214, 59);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(206, 53);
			this.button6.TabIndex = 2;
			this.button6.Text = "Cloud";
			this.button6.UseVisualStyleBackColor = false;
			this.button6.Click += new System.EventHandler(this.Button6_Click);
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.SystemColors.ActiveCaption;
			this.button5.Location = new System.Drawing.Point(214, 0);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(206, 53);
			this.button5.TabIndex = 1;
			this.button5.Text = "Small Target";
			this.button5.UseVisualStyleBackColor = false;
			this.button5.Click += new System.EventHandler(this.Button5_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.back);
			this.Controls.Add(this.dir);
			this.Controls.Add(this.chooseDir);
			this.Controls.Add(this.info);
			this.Controls.Add(this.Skip);
			this.Controls.Add(this.imageBox);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.Button1);
			this.Controls.Add(this.panel1);
			this.Name = "Form1";
			this.Text = "Boat Tagger";
			this.Load += new System.EventHandler(this.Form1_Load);
			((System.ComponentModel.ISupportInitialize)(this.imageBox)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.PictureBox imageBox;
        private System.Windows.Forms.Button Skip;
        private System.Windows.Forms.RichTextBox info;
        private System.Windows.Forms.Button chooseDir;
        private System.Windows.Forms.TextBox dir;
        private System.Windows.Forms.FolderBrowserDialog Browser;
        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
    }
}

